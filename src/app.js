const Board = require("./board");
// we want this global for the moment because this is where the whole interface lives
const readline = require("readline").createInterface({
  input: process.stdin,
  output: process.stdout
});

module.exports = function App() {
  /**
   * Runs the application
   */
  this.prompt = function() {
    readline.question(`input:`, command => {
      // this is VERY verbose but for a simple app like this,
      // it makes the barrier to entry for smaller
      try {
        if (command.includes("PLACE")) {
          // split the command from the args
          const parts = command.split(" ");
          if (parts.length === 1) {
            throw Error("No ARGS Supplied to PLACE Command");
          }
          // split the args
          const args = parts[1].split(",");
          this.handlePlace(...args);
        } else if (command === "MOVE") {
          this.handleMove();
        } else if (command === "LEFT") {
          this.handleLeft();
        } else if (command === "RIGHT") {
          this.handleRight();
        } else if (command === "REPORT") {
          this.handleReport();
        } else {
          throw Error("Invlaid Input!");
        }
      } catch (err) {
        console.log(err.message);
      }
      // keep prompting til we close the app
      this.prompt();
    });
  };

  /**
   * handles the place command
   * @param {*} x
   * @param {*} y
   * @param {*} direction
   */
  this.handlePlace = function(x, y, direction) {
    // make sure we're trying to get numbers in for the initial placement
    this.board = new Board(parseInt(x, 10), parseInt(y, 10), direction);
  };

  /**
   * handles the left command
   */
  this.handleLeft = function() {
    this.board.left();
  };

  /**
   * handles the right command
   */
  this.handleRight = function() {
    this.board.right();
  };

  /**
   * handles the move command
   */
  this.handleMove = function() {
    this.board.move();
  };

  /**
   * handles the report command
   */
  this.handleReport = function() {
    this.board.report();
  };
};
