/**
 * Board Object
 * this class handles the movement of the robot during the session
 */
const directions = ["NORTH", "SOUTH", "EAST", "WEST"];

module.exports = function Board(x, y, direction) {
  this.isValidCoOrdinate = function(coOrd) {
    // personal preference to be explicit with number ranges
    return coOrd > -1 && coOrd < 5;
  };

  this.isValidDirection = function(direction) {
    return directions.includes(direction);
  };

  this.canMove = function(x, y) {
    return this.isValidCoOrdinate(x) && this.isValidCoOrdinate(y);
  };

  this.move = function() {
    // again, VERY verbose, but theres only four options the check on
    // so this is easier to read and scale's well enough
    if (this.direction === "NORTH" && this.canMove(this.x, this.y + 1)) {
      this.y++;
    } else if (this.direction === "SOUTH" && this.canMove(this.x, this.y - 1)) {
      this.y--;
    } else if (this.direction === "EAST" && this.canMove(this.x, this.y + 1)) {
      this.x++;
    } else if (this.direction === "WEST" && this.canMove(this.x, this.y - 1)) {
      this.x--;
    } else {
      throw Error("Invalid Move! Your Robot will fall off the board");
    }
  };

  this.left = function() {
    // again, VERY verbose, but theres only four options the check on
    // so this is easier to read and scale's well enough
    if (this.direction === "NORTH") {
      this.direction = "WEST";
    } else if (this.direction === "SOUTH") {
      this.direction = "EAST";
    } else if (this.direction === "EAST") {
      this.direction = "NORTH";
    } else if (this.direction === "WEST") {
      this.direction = "SOUTH";
    }
  };

  this.right = function() {
    // again, VERY verbose, but theres only four options the check on
    // so this is easier to read and scale's well enough
    if (this.direction === "NORTH") {
      this.direction = "EAST";
    } else if (this.direction === "SOUTH") {
      this.direction = "WEST";
    } else if (this.direction === "EAST") {
      this.direction = "SOUTH";
    } else if (this.direction === "WEST") {
      this.direction = "NORTH";
    }
  };

  this.report = function() {
    console.log(`${this.x},${this.y},${this.direction}`);
  };

  // assign our object properties
  this.x = this.isValidCoOrdinate(x) ? x : false;
  this.y = this.isValidCoOrdinate(y) ? y : false;
  this.direction = this.isValidDirection(direction) ? direction : false;
  // we're dealing with 0 values so === it is!
  if (this.x === false || this.y === false || this.direction === false) {
    throw Error("Please Supply Co-ordinates and a direction e.g. 2,3,NORTH");
  }
};
