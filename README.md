# Toy Robot

## TL;DR
- clone the repo
- run `npm run start`

## Requirements
- this app is build using node 10. please use that version of node to run this app
- this app was built on ubuntu linux and is not tested with windows, it should work however there may be issues, please contact the author if there are issues running on windows

### Tests
tests are covered on the library only, you can run them by installing the dependencies and runnning npm run test

`npm i && npm run test`
