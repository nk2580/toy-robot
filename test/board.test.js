/**
 * Board Test
 */

const Board = require("../src/board");

describe("board.js", () => {
  describe("construct", () => {
    it("returns an instance of itself", () => {
      const board = new Board(0, 0, "NORTH");
      expect(board).toBeInstanceOf(Board);
    });
    it("assigns its props from the inout", () => {
      const board = new Board(2, 0, "NORTH");
      expect(board.direction).toBe("NORTH");
      expect(board.x).toBe(2);
      expect(board.y).toBe(0);
    });
    it("throws an error with co-ords params larger than 4 and less than 0", () => {
      const tester = () => {
        const board = new Board(-1, 0, "NORTH");
      };
      expect(tester).toThrow();
      const tester2 = () => {
        const board = new Board(0, 5, "NORTH");
      };
      expect(tester2).toThrow();
    });

    it("throws an error Directions other then NORTH SOUTH EAST WEST", () => {
      const tester = () => {
        const board = new Board(-1, 0, "BAD");
      };
      expect(tester).toThrow();
    });
  });
  describe("moving", () => {
    it("should check if we can move", () => {
      const board = new Board(0, 0, "NORTH");
      board.canMove = jest.fn().mockReturnValue(true);
      board.move();
      expect(board.canMove).toBeCalled();
    });
    it("should throw an error if we cant move", () => {
      const board = new Board(0, 0, "NORTH");
      board.canMove = jest.fn().mockReturnValue(false);
      const t = () => board.move();
      expect(t).toThrow();
    });
    it("should Move the robot", () => {
      const cases = [
        { input: [0, 0, "NORTH"], expected: { y: 1, x: 0 } },
        { input: [0, 0, "EAST"], expected: { y: 0, x: 1 } },
        { input: [4, 4, "SOUTH"], expected: { y: 3, x: 4 } },
        { input: [4, 4, "WEST"], expected: { y: 4, x: 3 } }
      ];
      cases.forEach(({ input, expected }) => {
        const board = new Board(...input);
        board.move();
        expect(board.x).toBe(expected.x);
        expect(board.y).toBe(expected.y);
      });
    });
  });
  describe("right", () => {
    it("should turn the robot right", () => {
      const cases = [
        { input: [0, 0, "NORTH"], expected: { direction: "EAST" } },
        { input: [0, 0, "EAST"], expected: { direction: "SOUTH" } },
        { input: [0, 0, "SOUTH"], expected: { direction: "WEST" } },
        { input: [0, 0, "WEST"], expected: { direction: "NORTH" } }
      ];
      cases.forEach(({ input, expected }) => {
        const board = new Board(...input);
        board.right();
        expect(board.direction).toBe(expected.direction);
      });
    });
  });
  describe("left", () => {
    it("should turn the robot left", () => {
      const cases = [
        { input: [0, 0, "NORTH"], expected: { direction: "WEST" } },
        { input: [0, 0, "WEST"], expected: { direction: "SOUTH" } },
        { input: [0, 0, "SOUTH"], expected: { direction: "EAST" } },
        { input: [0, 0, "EAST"], expected: { direction: "NORTH" } }
      ];
      cases.forEach(({ input, expected }) => {
        const board = new Board(...input);
        board.left();
        expect(board.direction).toBe(expected.direction);
      });
    });
  });

  describe("report", () => {
    it("should print the current state of the session", () => {
      const board = new Board(2, 4, "WEST");
      global.console = { log: jest.fn() };
      board.report();
      expect(console.log).toBeCalled();
    });
  });
});
